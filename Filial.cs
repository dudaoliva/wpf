﻿using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1
{
    public class Filial
    {

        public string FilialCOD { get; set; }
        public string FilialIP { get; set; }
        public string FilialNome { get; set; }
    }


    public class FilialMap : ClassMap<Filial>
    {
        public FilialMap()
        {
            Map(m => m.FilialCOD).Name("FilialCOD"); // *obrig
            Map(m => m.FilialIP).Name("FilialIP"); // *obrig
            Map(m => m.FilialNome).Name("FilialNome"); // *obrig
        }
    }
}
